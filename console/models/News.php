<?php

namespace console\models;


use Yii;
use common\components\StringHelper;

/**
 * Lesson 10. Handles getting, preparing new from database
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class News {
    
    /**
     * <b>to get news list from a database</b>
     */
    public static function getNewsList(){
        
        $sql = 'SELECT * FROM news;';
        
        $result = Yii::$app->db->createCommand($sql)->queryAll();
        return self::prepareNewsList($result);
        
    }
    
    /**
     * <b>to prepare news list for receiving</b>
     * @param array $list
     */
    protected  static function prepareNewsList($result){
         if (!empty($result) && is_array($result)){
             
            foreach ($result as &$item){
                
                $item['content'] = Yii::$app->stringHelper->getShort($item['content'], 220);
            }
        }        
        return $result;
    }
}
