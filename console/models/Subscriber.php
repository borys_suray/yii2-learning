<?php

namespace console\models;

use Yii;

/**
 * Lesson 10. Handles getting subscribers' list from database
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class Subscriber {
    
    /**
     * @return array|boolean
     */
    public static function getSubscribersList(){
        
        $sql = 'SELECT * FROM subscriber WHERE true';
        $result = Yii::$app->db->createCommand($sql)->queryAll();
        
        if (!is_array($result)){
            return false;
        }
        $emails = array();
        
        foreach ($result as $subscriber){
            $emails[$subscriber['id']] = $subscriber['email'];
        }
        
        return $emails;
    }
}
