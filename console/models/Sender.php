<?php

namespace console\models;

use Yii;

/**
 * Lesson 10. Handles sending email
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class Sender
{
    public static function run($newsList, $subscribers){
        
        foreach ($subscribers as $id => $email){
            $result = Yii::$app->mailer->compose('/mail/newslist',
                    ['newslist' => $newsList])
                ->setFrom('boryssuray@gmail.com')
                ->setTo($email)
                ->setSubject('Next Mail from console')
                ->send();
        var_dump($result);
        }
    }
}