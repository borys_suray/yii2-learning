<?php

use yii\db\Migration;

/**
 * Class m180204_110314_alter_employee_table
 * add columns 'birthDate', 'startDate', 'position', 'cityId', 'idCode' to the employee table
 */
class m180204_110314_alter_employee_table extends Migration
{
    
    public function up()
    {
        $this->addColumn('employee', 'birthDate', 'date');
        $this->addColumn('employee', 'startDate', 'date');
        $this->addColumn('employee', 'position', 'string');
        $this->addColumn('employee', 'cityId', 'int');
        $this->addColumn('employee', 'idCode', 'string');   
    }

    public function down()
    {
        $this->dropColumn('employee', 'birthDate');
        $this->dropColumn('employee', 'startDate');
        $this->dropColumn('employee', 'position');
        $this->dropColumn('employee', 'cityId');
        $this->dropColumn('employee', 'idCode');
    }
}
