<?php

use yii\db\Migration;

/**
 * Class m180128_121621_drop_city_column
 */
class m180128_121621_drop_city_column extends Migration
{
   
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->dropColumn('employee', 'city');
    }

    public function down()
    {
       $this->addColumn('employee', 'city', 'string');
    }
}
