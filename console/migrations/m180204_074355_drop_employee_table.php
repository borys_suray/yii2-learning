<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `employee`.
 */
class m180204_074355_drop_employee_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropTable('employee');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->createTable('employee', [
            'id' => $this->primaryKey(),
        ]);
    }
}
