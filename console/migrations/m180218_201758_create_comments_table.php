<?php

use yii\db\Migration;

/**
 * Handles the creation of table `comments`.
 */
class m180218_201758_create_comments_table extends Migration
{
    /**
     * @create table comments to store comments
     */
    public function up()
    {
        $this->createTable('comments', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'email' => $this->string(),
            'comment' => $this->string(),
        ]);
    }

    /**
     * delete table comments
     */
    public function down()
    {
        $this->dropTable('comments');
    }
}
