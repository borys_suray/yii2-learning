<?php

use yii\db\Migration;

/**
 * Lesson 15. Home-task.
 * Handles the creation of table `category`.
 */
class m180223_182747_create_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('category');
    }
}
