<?php

use yii\db\Migration;

/**
 * Class m180128_120451_add_salary_column
 */
class m180128_120451_add_salary_column extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $sql = 'ALTER TABLE employee ADD COLUMN salary INT(10);';
        
        Yii::$app->db->createCommand($sql)->execute();
               
    }

    public function down()
    {
        $sql = 'ALTER TABLE employee DROP COLUMN salary;';
        
        Yii::$app->db->createCommand($sql)->execute();
    }
    
}
