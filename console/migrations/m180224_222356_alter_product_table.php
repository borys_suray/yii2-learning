<?php

use yii\db\Migration;

/**
 * Class m180224_222356_alter_product_table
 */
class m180224_222356_alter_product_table extends Migration
{
    public function up()
    {
        $this->dropColumn('product', 'image');
    }

    public function down()
    {
        $this->addColumn('product', 'image', 'binary');
    }
}
