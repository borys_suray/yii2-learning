<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cities`.
 */
class m180220_171819_create_cities_table extends Migration
{
    /**
     * create table cities
     */
    public function up()
    {
        $this->createTable('cities', [
            'cityId' => $this->primaryKey(),
            'city' => $this->string(),
        ]);
    }

    /**
     * delete table cities
     */
    public function down()
    {
        $this->dropTable('cities');
    }
}
