<?php

use yii\db\Migration;

/**
 * Lesson 15. Home-task.
 * Handles the creation of table `producer`.
 */
class m180223_183020_create_producer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('producer', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'country' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('producer');
    }
}
