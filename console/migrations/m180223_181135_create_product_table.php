<?php

use yii\db\Migration;

/**
 * LEsson 15. Home-task.
 * Handles the creation of table `product`.
 */
class m180223_181135_create_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'description' => $this->string(),
            'image' => $this->binary(),
            'price' => $this->integer(),
            'is_available' => $this->integer(1),
            'category_id' => $this->integer(),
            'producer_id' => $this->integer(),            
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('product');
    }
}
