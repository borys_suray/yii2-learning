<?php

use yii\db\Migration;

/**
 * Handles the creation of table `employee`.
 */
class m180128_115050_create_employee_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('employee', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->unique(),
            'city' => $this->string(),
            'works_from' => $this->date(),
            'position' => $this->string(),
            'practice' => $this->integer(),
            'id_code' => $this->integer(),
            'department_id' => $this->integer(),
            'email' => $this->string(),           
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('employee');
    }
}
