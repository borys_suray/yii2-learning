<?php

use yii\db\Migration;

/**
 * Handles the creation of table `employee`.
 */
class m180204_075010_create_employee_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('employee', [
            'id' => $this->primaryKey(),
            'firstName' => $this->string(),
            'lastName' => $this->string(),
            'middleName' => $this->string(),
            'email' => $this->string(),            
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('employee');
    }
}
