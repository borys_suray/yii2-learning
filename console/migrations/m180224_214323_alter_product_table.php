<?php

use yii\db\Migration;

/**
 * Class m180224_214323_alter_product_table
 */
class m180224_214323_alter_product_table extends Migration
{
   
    public function up()
    {
        $this->addColumn('product', 'name', 'string');
    }

    public function down()
    {
        $this->dropColumn('product', 'name');
    }
}
