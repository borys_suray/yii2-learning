<?php

namespace console\controllers;

/**
 * Lesson 10. Console apps
 * 
 * @author Borys Suray <surayborys@gmail.com>
 */
class AnotherController extends \yii\console\Controller
{
    /**
     * To test console applications
     */
    public function actionTest(){
        echo 'Test:: Hello World!!!';
        die;
    }   
}
