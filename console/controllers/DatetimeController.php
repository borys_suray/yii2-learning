<?php

namespace console\controllers;

use Yii;

/**
 * Lesson 10. Cron. Handles writing current timedate to lo file
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class DatetimeController extends \yii\console\Controller
{
    
    /**
     * <b>To wtite current timedate to the log.txt file</b>
     * @return boolean|void
     */
    public function actionWrite() {
        
        $file = fopen('/var/www/project/frontend/web/log.txt', 'a');
        
        if (!$file){
            return false;
        }
        
        $string = Yii::$app->formatter->asDatetime('now'); 
        
        $write = fwrite($file, $string . PHP_EOL);
        
        fclose($file);
        
        if (!$write){
            return false;
        }        
    }
}
