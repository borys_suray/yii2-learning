<?php

namespace console\controllers;

use Yii;
use console\models\News;
use console\models\Subscriber;
use console\models\Sender;

/**
 * Lesson 10. Console apps
 * 
 * @author Borys Suray <surayborys@gmail.com>
 */
class MailController extends \yii\console\Controller
{
    /**
     * To test sending email via console
     */
    public function actionSend(){
        
        $newsList = News::getNewsList();
        
        $subscribers = Subscriber::getSubscribersList();
        
        $result = Sender::run($newsList, $subscribers);
        die;
        
    }
}

