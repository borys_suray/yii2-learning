<?php

/* 
 * Lesson 13. Output buffering
 */

ob_start(); //start buffering

echo 'Hello<br>';
echo 'Hello<br>';
echo 'Hello<br>';
echo 'Hello<br>';
echo 'Hello<br>';
echo 'Hello<br>';

$content = ob_get_contents(); //get buffer content

ob_clean(); //clean buffer

$content = strtr($content, 'o', 'O'); //change content

echo $content; //display content