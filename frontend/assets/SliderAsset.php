<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Lesson 12. Assets
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class SliderAsset extends AssetBundle {
    
    public $css = [
        'css/slider/slider.css',
    ];
    
    public  $js = [
        'js/responsiveslide/responsiveslides.min.js',
    ];
     
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
