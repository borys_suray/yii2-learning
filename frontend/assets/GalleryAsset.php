<?php


namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Lesson 12. Handles connection to resources for creating gallery
 * (css style & javascript isotope library)
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class GalleryAsset extends AssetBundle {
    
    public $css = [
        'css/gallery/gallery.css',
    ];
    
    public  $js = [
        'js/isotope/isotope.js',
    ];
     
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
