<?php

namespace frontend\controllers;

use frontend\controllers\behaviors\AccessBehavior;
use yii\filters\VerbFilter;
use frontend\models\Author;
use Yii;

/**
 *  Lesson 16. CRUD
 * 
 *  @author Borys Suray <surayborys@gmail.com>
 */
class AuthorController extends \yii\web\Controller
{
    /**
     *  include behaviors
     */
    public function behaviors() {
        return
            [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            AccessBehavior::className(),
        ];
    }

        /**
    * to render the form for adding author to the view frontend\views\author\create
    * to write a new author to the table "author"
    * 
    * @return type mixed
    */
    public function actionCreate()
    {
        $model = new Author;
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            
            Yii::$app->session->setFlash('success', 'New author successfully added');
            return $this->redirect(['author/index']);
        }
        
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
                
        $model->delete();
            
        Yii::$app->session->setFlash('success', 'Author successfully deleted');
        return $this->redirect(['author/index']);
}
    
    /**
     * to render the authors list to the view frontend\views\author\index
     * 
     * @return mixed
     */
    public function actionIndex()
    {
        $author = new Author; 
        $authorsList = $author->find()->all();
        
        return $this->render('index', [
            'authorsList' => $authorsList,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = Author::findOne($id);
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            
            Yii::$app->session->setFlash('success', 'Author successfully apdated');
            return $this->redirect(['author/index']);
        }
        
        return $this->render('update', [
            'model' => $model,
        ]);
    }
    
    public function findModel($id){
        if (($model = Author::findOne($id)) !== null) {
            return $model;
        }

        return $this->redirect(['site/index']);
    }
    
}
