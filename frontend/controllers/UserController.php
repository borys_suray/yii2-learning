<?php

namespace frontend\controllers;

use frontend\models\forms\SignupForm;
use frontend\models\forms\LoginForm;
use Yii;

/**
 * <b>Identity class for Yii\web\User component</b>
 * <p>Handles interaction with user</p>
 */
class UserController extends \yii\web\Controller
{
    public function actionLogin()
    {
        $model = new LoginForm();
        
        if ($model->load(Yii::$app->request->post()) && $model->login()){
            
            Yii::$app->session->setFlash('success', 'Welcome');
            return $this->redirect('/site/index');
        }
        
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        
        if($model->load(Yii::$app->request->post()) && $user = $model->save()){
            
            //login registered user
            Yii::$app->user->login($user);
            
            Yii::$app->session->setFlash('success', 'A new user has been registered');
            return $this->redirect('/site/index');
        }
        
        return $this->render('signup', [
            'model' => $model,
        ]);
    }
    
    public function actionLogout(){
        
        Yii::$app->user->logout();
        
        return $this->redirect(['site/index']);
    }

}
