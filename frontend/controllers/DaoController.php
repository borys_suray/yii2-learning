<?php


namespace frontend\controllers;
use yii\web\Controller;
use Yii;

/**
 * Lesson 14. DAO
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class DaoController extends Controller {
    
    public function actionDao(){
        
        //1. Create object - connection to DB
        $db = new \yii\db\Connection([
            'dsn' => 'mysql:host=localhost;dbname=yii2advanced',
            'username' => 'yii2user',
            'password' => '111111',
            'charset' => 'utf8',
        ]);
        
        //2. Create object - new sql command
        $sql = 'SELECT * FROM cities;';
        
        $command = new \yii\db\Command([
            'db' => $db,
            'sql' => $sql,
        ]);
        
        //3. Execution of the command
        $arrayWithResults = $command->queryAll();
        
        //4. Test querying from two databases
        $sql2 = 'SELECT * FROM user';
        $result2 = Yii::$app->db2->createCommand($sql2)->queryAll();
        
       
        return $this->render('index', [
            'db' => $db,
            'command' => $command,
            'arrayWithResults' => $arrayWithResults,
            'arr2' => $result2,
        ]);
    }
}
