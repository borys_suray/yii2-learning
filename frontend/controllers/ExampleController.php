<?php

namespace frontend\controllers;
use yii\base\Controller;
use frontend\models\examples\Human;
use frontend\models\examples\Cat;

/**
 * To demonstrate how traits work
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class ExampleController extends Controller {
    
    public function actionIndex(){
        $cat = new Cat();
        $human = new Human();
        
        $cat->walk();
        $human->walk();
    }
}
