<?php


namespace frontend\controllers;

use yii\web\Controller;

/**
 * Lesson 13. HTML Helper
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class HtmlHelperController extends Controller{
    
    public function actionDemo(){ 
        return $this->render('html-helper');
    }
    
    public function actionEscapeOutput() {
        
        //example - simple comments
        $comments = [
            [
                'id' => 10,
                'author' => 'Student',
                'text' => 'Hello',
            ],
            [
                'id' => 11,
                'author' => 'Victor',
                'text' => 'Hello. How are you?',
            ],
            [
                'id' => 11,
                'author' => 'Hacker',
                'text' => '<b>Hello</b><script>alert(`I\'ll still your money!!!`);</script>',
            ],
        ];
        
        return $this->render('escape-output',[
                    'comments'=>$comments
                ]);
    }
}
