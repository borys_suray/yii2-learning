<?php


namespace frontend\controllers;
use yii\web\Controller;
use frontend\models\Comment;
use Yii;

/**
 * Handles comments list rendering and adding new comment
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class CommentController extends Controller{

    public function actionRun(){
        
        $model = new Comment();
        $model->scenario = Comment::SCENARIO_COMMENT_ADD;
        
        $formData = Yii::$app->request->post();
        
        if(Yii::$app->request->isPost) {
            
            //use bulk assigment
            $model->attributes = $formData;
            if ($model->validate() && $model->add()){
                //set flash message 'success'
                Yii::$app->session->setFlash('success', 'your comment added');
            }
        }
        
        return $this->render('comments', [
            'model' => $model
            ]);
    }
}
