<?php

namespace frontend\controllers\behaviors;

use yii\base\Behavior;
use yii\web\Controller;
use Yii;

/**
 * AccessBehavior
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class AccessBehavior extends Behavior{
    
    /**
     * connects the behavior with default yii\web\Controller events
     * 
     * @return array
     */
    public function events() {
        return[
            Controller::EVENT_BEFORE_ACTION => 'checkAccess',
        ];
    }


    /**
     *  to check if the user is guest
     */
    public function checkAccess()
    {
        if(Yii::$app->user->isGuest) {
            return Yii::$app->controller->redirect(['site/index']);
        }
    }
}
