<?php

namespace frontend\controllers;

use frontend\models\Book;
use frontend\models\Publisher;
use Yii;

/**
 * Lesson 15. ActiveRecord
 * 
 * @author Borys Suray <surayborys@gmail.com>
 */

class BookshopController extends \yii\web\Controller
{
    /**
     * test function
     */
    public function actionIndex()
    {
        
        //$conditions = ['publisher_id' => [3, 77]];
        $conditions = true;
        $orderConds = ['date_published' => SORT_DESC];
        $limit = 20;
        $bookList = Book::find()->where($conditions)->limit($limit)->orderBy($orderConds)->all();
        
        return $this->render('index', [
           'bookList' => $bookList,
        ]);
    }
    
    /**
     * to insert a new record to database
     * @return mixed
     */
    public function actionAdd(){
        
        $book = new Book();
        $publishers = Publisher::getAll();
        
        if($book->load(Yii::$app->request->post()) && $book->save()) {
            
            //set flash message 'success'
            Yii::$app->session->setFlash('success', 'Added new book');
            //return $this->redirect(['bookshop/index']);
            return $this->refresh();
        }
        
        return $this->render('add', [
            'book' => $book,
            'publishers' => $publishers,
        ]);
    }

}
    