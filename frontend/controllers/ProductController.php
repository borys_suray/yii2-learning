<?php

namespace frontend\controllers;
use frontend\models\Product;
use Yii;

/**
 * <b>ProductController</b>
 * <p>Controller for work with products (lesson 15. ActiveRecord. Home Task)</p>
 * 
 * @author Borys Suray <surayborys@gmail.com>
 */
class ProductController extends \yii\web\Controller
{
    public function actionIndex()
    {
         $productsList = Product::find()->all();
        
        
        return $this->render('index', [
            'productsList' => $productsList,
        ]);
    }
    
    public function actionAdd()
    {
        $product = new Product;
        $product->scenario = Product::SCENARIO_PRODUCT_ADD;
        
        if($product->load(Yii::$app->request->post()) && $product->save())
        {
            //set flash message 'success'
            Yii::$app->session->setFlash('success', 'Added new product');
            //return $this->redirect(['bookshop/index']);
            return $this->refresh();
        }
        
        return $this->render('add', [
            'product' => $product,
        ]);
    }

}
