<?php

namespace frontend\controllers;

use yii\web\Controller;
use frontend\models\Employee;

/**
 * Lesson 13. Demonstrates ArrayHelpers' possibilities
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class ArrayHelperController extends Controller {
    
    public function actionDemo(){
        $employees = Employee::getEmployeeList();
        
        return $this->render('demo', [
            'employees' => $employees,
        ]);
    }
}
