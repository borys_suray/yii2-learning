<?php

namespace frontend\controllers;

use yii\web\Controller;
use frontend\models\Test;
#use frontend\components\StringCutter;
#use frontend\components\StringCutter;
use Yii;
use Faker\Factory;
use frontend\models\News;

/**
 * @author boryssuray <surayborys@gmail.com>
 */
class TestController extends Controller
{
    /**
     * <b>to get news list and return it to view</b>
     * @return array
     */
    public function actionIndex(){
        
        $newsList = Test::getNewsList();
        $email = Yii::$app->params['adminEmail'];
        
              
        return $this->render('renamed', [
            'list'=>$newsList,
            'email'=>$email,
        ]);
    }
    
   public function actionGenerate(){
       /**
        * @var $faker Faker\Generator instance 
        */
        $faker = Factory::create();
        
        for($i=0; $i<10; $i++)
        {
            $news = []; 
            
            for($j=0; $j<100; $j++){
                $news[] = [$faker->text(35),$faker->text(rand(100, 120)),  $faker->text(rand(1000, 2000))];
            }
            
            Yii::$app->db->createCommand()->batchInsert('news', ['title', 'description', 'content'], $news)->execute();
            unset($news);
        }        
   }
}

