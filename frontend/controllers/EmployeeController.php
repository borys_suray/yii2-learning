<?php


namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\Employee;

/**
 * Lesson 11. Models
 * Lesson 14. Updated (using $model->load instead of $formData = Yii::$app->request->post(); in actionRegister)
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class EmployeeController extends Controller {
    
    /**
     * to demonstrate some advntages of yii\base\Model class
     */
    public function actionIndex(){
        
        /*$employee = new Employee();
        
        $employee->name = 'Borys';
        $employee->age = 31;
        $employee->position = 'senjor php developer';
        $employee->salary = 3500;
        
        foreach ($employee as $attr => $val){
            echo $attr . ': ' . $val . '</br>';
        }
        
        echo '<hr>';
        
        $array = $employee->toArray();
        print_r($array);
        
        echo '<br>';
        
        print_r($employee->getAttributes());
        
        echo '<br>';
        
        print_r($employee->attributes);
        
        echo '<br>';
        
        print_r($employee->attributes());*/
    }
    /**
     * handles new employee registration
     * @return type
     */
    public function actionRegister(){
        
        $model = new Employee;
        $model->scenario = Employee::SCENARIO_EMPLOYEE_REGISTER;
        
       
        if($model->load(Yii::$app->request->post())){
                       
            if ($model->validate() && $model->save()){
                //set flash message 'success'
                Yii::$app->session->setFlash('success', 'Registered');
            }
        }
        
        return $this->render('register', [
            'model' => $model,
        ]);
    }
    
    /**
     * handles ipdating employee info
     * @return type
     */
    public  function actionUpdate(){
        
        $model = new Employee;
        $model->scenario = Employee::SCENARIO_EMPLOYEE_UPDATE;
        
        $formData = Yii::$app->request->post();
       
        if(Yii::$app->request->isPost){
            
            //use bulk assigment
            $model->attributes = $formData;
            if ($model->validate() && $model->update()){
                //set flash message 'success'
                Yii::$app->session->setFlash('success', 'Updated');
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }
    
    
}
