<?php

namespace frontend\controllers;

use yii\web\Controller;
use frontend\models\WindowOrder;
use Yii;


/**
 * Lesson 11. Handles window order
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class OrderController extends Controller{
     
    public function actionOrder(){
        
        $model = new WindowOrder;
        $model->scenario = WindowOrder::SCENARIO_WINDOW_ORDER_ORDER;
        
        $formData = Yii::$app->request->post();
        
        if (Yii::$app->request->isPost){
            
            //use bulk assigment and validate data
            $model->attributes = $formData;
            
            if ($model->validate() && $model->sendOrderToAdmin()){
                //set flash message 'success'
                Yii::$app->session->setFlash('success', 'Registered');
            }
        }
        
        return $this->render('window-order', [
            'model' => $model,
        ] );
    }
}
