<?php

namespace frontend\controllers;

use yii\web\Controller;
use frontend\models\Count;

/**
 * <b>contains function to count items and generate view</b>
 * <p>(lesson 9. Yii Application)</p>
 * @author borys suray <surayborys2gmail.com>
 */
class CountController  extends Controller {
    
    /**
     * 
     * @return boolean|array
     */
    public function actionCount(){
        
        $numberOfNews = Count::countItems();
        
        if ($numberOfNews == false){
            return false;
        }
        
        return $this->render('counter', [
            'num'=>$numberOfNews,
        ]);
    }  
}
