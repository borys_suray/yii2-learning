<?php

namespace frontend\controllers;

use yii\web\Controller;

/**
 * Lesson 12. Assets
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class GalleryController extends Controller {
    
    public function actionShow(){
        
        return $this->render('gallery');
    }
}
