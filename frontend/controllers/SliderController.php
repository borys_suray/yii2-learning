<?php

namespace frontend\controllers;
use yii\web\Controller;

/**
 * Lesson 12. Widgets
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class SliderController extends Controller{
    /**
     * handles rendering view 
     */
    public function actionRun() {
        
        return $this->render('slider');
    }
}
