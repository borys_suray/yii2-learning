<?php

namespace frontend\controllers;

use frontend\models\Producer;
use Yii;

/**
 * <b>ProducerController</b>
 * <p>Controller for work with producers (lesson 15. ActiveRecord. Home Task)</p>
 * 
 * @author Borys Suray <surayborys@gmail.com>
 */
class ProducerController extends \yii\web\Controller
{
    /**
     * function to get producers list
     * 
     * @return mixed
     */
    public function actionIndex()
    {
        $producersList = Producer::find()->all();
        
        return $this->render('index', [
            'producersList' => $producersList,
        ]);
    }
    
    /**
     * function for adding a new producer to table "producer"
     * 
     * @return mixed
     */
    public function actionAdd()
    {
        $producer = new Producer;
        $producer->scenario = Producer::SCENARIO_PRODUCER_ADD;
        
        if($producer->load(Yii::$app->request->post()) && $producer->save())
        {
            //set flash message 'success'
            Yii::$app->session->setFlash('success', 'Added new producer');
            //return $this->redirect(['bookshop/index']);
            return $this->refresh();
        }
        
        return $this->render('add', [
            'producer' => $producer,
        ]);
    }

}
