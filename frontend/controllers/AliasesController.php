<?php

namespace frontend\controllers;

use yii\web\Controller;
use Yii;

/**
 * Lesson 12. Using aliases
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class AliasesController extends Controller 
{
    /**
     *  test function
     */
    public function actionExample(){
       $result = mkdir(Yii::getAlias('@public') . '/files'); 
       var_dump($result);
       
       $result2 = mkdir(Yii::getAlias('@files') . '/photos');
       var_dump($result2);
    }
}
