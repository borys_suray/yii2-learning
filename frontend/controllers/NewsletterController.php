<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\Subscribe;

/**
 * Lesson 11. Handles validating user input end executing subscribe
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class NewsletterController extends Controller
{
    
    /**
     * <p>to get data from the form, validate them and write to database.subscriber</p>
     * @return mixed
     */
    public function actionSubscribe(){
        
        $formData = Yii::$app->request->post();
        $model = new Subscribe;
        
        if (Yii::$app->request->isPost) {
                       
            $model->email = $formData['email'];
            if ($model->validate() && $model->save()){
                Yii::$app->session->setFlash('success', 'Subscribe completed!');
            }
        }
                
        return $this->render('subscriber',[
                    'model' => $model,
        ]);
    }
}
