<?php

namespace frontend\controllers;

use frontend\models\Category;
use Yii;

/**
 * <b>CategoryController</b>
 * <p>Controller for work with categories (lesson 15. ActiveRecord. Home Task)</p>
 * 
 * @author Borys Suray <surayborys@gmail.com>
 */
class CategoryController extends \yii\web\Controller
{
    /**
     * function to get categories list
     * 
     * @return mixed
     */
    public function actionIndex()
    {
        $categoriesList = Category::find()->all();
        
        
        return $this->render('index', [
            'categoriesList' => $categoriesList,
        ]);
    }
    
    /**
     * function for adding a new category to table "category"
     * 
     * @return mixed
     */
    public function actionAdd()
    {
        $category = new Category;
        $category->scenario = Category::SCENARIO_CATEGORY_ADD;
        
        if($category->load(Yii::$app->request->post()) && $category->save())
        {
            //set flash message 'success'
            Yii::$app->session->setFlash('success', 'Added new category');
            //return $this->redirect(['bookshop/index']);
            return $this->refresh();
        }
        
        return $this->render('add', [
            'category' => $category,
        ]);
    }

}
