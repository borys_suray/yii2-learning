<?php

namespace frontend\components;

use Yii;

/** 
 * <b>contains methods for cutting strings</b>
 * @author borys suray <surayborys@gmail.com>
 */
class StringCutter
{
    protected $limit;
    
    public function __construct() {
        $this->limit = Yii::$app->params['shortTextLimit'];
    }
    
    /**
     * <b>to get string, cutted to first $limit characters (until the first space)</b>
     * @param string $string
     * @param mixed $limit
     * @return string|boolean
     */
    public function getShortWithSpace($string, $limit = null){
        
        if(!is_string($string)){   
            return false;
        }
        
        if($limit === null){
            $limit = $this->limit;
        }
        $limit = intval($limit);
        
        $chars_arr = str_split($string, $limit);
                
        $begin = array_shift($chars_arr);
        $end = implode($chars_arr);
        
                
        if(preg_match('~^(.*)?(\s){1,}$~', $begin)){
            return rtrim($begin);
        }
        else {
            $position = strpos($end, ' ');
                if ($position == false){
                    return $string;
                }
            $add_to_begin = substr($end, 0, $position);
            return $begin . $add_to_begin;
            
        }       
    }
    
    /**
     * <b>to get first $number words of string</b>
     * @param string $string
     * @param int $number
     * @return boolean|array
     */
    public function getFirstWords($string, $number = 1){
        
        if(!is_string($string)){   
            return false;
        }
        
        $int_number = intval($number);       
        
        $words_array = explode(' ', $string, $int_number+1);
        
        array_pop($words_array);  
        
        return implode($words_array, ' ');
    }
}

