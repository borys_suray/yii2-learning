<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'enableCsrfValidation' => false,
        ],
        'user' => [
            'identityClass' => 'frontend\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'articles' => 'test/index',
                'articles/<id:\d+>' => 'test/view',
                'count-items' => 'count/count',
                'subscribe' => 'newsletter/subscribe',
                'win-order' => 'order/order',
                'gallery' => 'gallery/show',
                'slider' => 'slider/run',
                'array-helper' => 'array-helper/demo',
            ],  
        ],
        'stringHelper' => [
            'class' => 'common\components\StringHelper',
        ],
        'stringCutter' => [
            'class' => 'frontend\components\StringCutter',
        ],       
    ],
    'params' => $params,
    'aliases' => [
        '@public' => '/var/www/project/frontend/web/',
        '@files' => '@public/files',
        '@images' => '/files/photos',
    ],
];
