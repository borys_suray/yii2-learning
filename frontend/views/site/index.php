<?php

/* @var $this yii\web\View
 * @var list frontend\controllers\SiteController */
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\widgets\newsList\NewsList;
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p>
            <a class="btn btn-lg btn-success" href="<?php echo Url::to(['newsletter/subscribe'])?>">
                Subscribe
            </a>
            <a class="btn btn-lg btn-success" href="<?php echo Url::to(['order/order'])?>">
                Order
            </a>
            <a class="btn btn-lg btn-success" href="<?php echo Url::to(['gallery/show'])?>">
                Gallery
            </a>
            <a class="btn btn-lg btn-success" href="<?php echo Url::to(['slider/run'])?>">
                Slider
            </a>
            <a class="btn btn-lg btn-success" href="<?php echo Url::to(['html-helper/demo'])?>">
                HTMLHelp 
            </a>
            <a class="btn btn-lg btn-success" href="<?php echo Url::to(['array-helper/demo'])?>">
                ArrayHelp 
            </a>
        </p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Search</h2>

                <p>
                    <a class="btn btn-primary" href="<?php echo Url::to(['search/index'])?>">
                        full-text mysql search
                    </a>
                </p>
                <p>
                    <a class="btn btn-primary" href="<?php echo Url::to(['search/advanced'])?>">
                        sphinx search
                    </a>
                </p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>News</h2>

                <p>
                    <?php echo NewsList::widget(['show_limit'=>2]); ?>
                </p>

            </div>
        </div>

    </div>
</div>
