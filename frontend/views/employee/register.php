<?php
/**
 * Lesson 11. Registration form
 * Lesson 14. Updated with ActiveForm
 *
 * @var $model frontend\models\Employee 
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

if ($model->hasErrors()){
    echo '<pre>';
    print_r($model->getErrors());
    echo '</pre>';
}

if (Yii::$app->session->hasFlash('success')){
    echo Yii::$app->session->getFlash('registerStatus');
}

?>

<?php $form = ActiveForm::begin(); ?>

<?php echo $form->field($model, 'firstName') ?>
<?php echo $form->field($model, 'middleName') ?>
<?php echo $form->field($model, 'lastName') ?>
<?php echo $form->field($model, 'email') ?>
<?php echo $form->field($model, 'birthDate') ?>
<?php echo $form->field($model, 'startDate') ?>
<?php echo $form->field($model, 'cityId')->dropDownList($model->getCitiesList()); ?>
<?php echo $form->field($model, 'position') ?>
<?php echo $form->field($model, 'idCode') ?>
<?php echo Html::submitButton('register', ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>


<!--START OLD FORM CODE-->

<!--form action="" method="post">
    <div class="form-group" style="width: 50%">
         <h2>WELCOME TO OUR COMPANY</h2><br>
        <input class="form-control" type="text" name ="firstName" placeholder="first name"><br>
        <input class="form-control" type="text" name ="middleName" placeholder="middle name"><br>
        <input class="form-control" type="text" name ="lastName" placeholder="last name"><br>
        <input class="form-control" type="text" name ="email" placeholder="email"><br>
        <input class="form-control" type="date" name ="birthDate" placeholder="birth date"><br>
        <input class="form-control" type="date" name ="startDate" placeholder="start date"><br>
        <select name ="cityId" class="form-control">
            <option value="1">Kyiv</option>
            <option value="2">Lviv</option>
            <option value="3">Kharkiv</option>
            <option value="4">Dnipro</option>
            <option value="5">Odessa</option>
            <option value="6">Another...</option>
        </select><br>
        <input class="form-control" type="text" name ="position" placeholder="position"><br>
        <input class="form-control" type="text" name ="idCode" placeholder="identification code"><br>
        <input  type="submit"class="btn btn-success form-control" value ="Register">
    </div>
</form-->