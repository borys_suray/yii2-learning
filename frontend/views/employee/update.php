<?php
/**
 * Lesson 11. Update form 
 *
 * @var $model frontend\models\Employee 
 */
if ($model->hasErrors()){
    echo '<pre>';
    print_r($model->getErrors());
    echo '</pre>';
}

if (Yii::$app->session->hasFlash('success')){
    echo Yii::$app->session->getFlash('updateStatus');
}
?>
<form action="" method="post">
    <h2>UPDATE EMPLOYEE DATA</h2>
    <input type="text" name ="firstName" placeholder="first name"><br><br>
    <input type="text" name ="middleName" placeholder="middle name"><br><br>
    <input type="text" name ="lastName" placeholder="last name"><br><br>
    <input type="submit" value ="Update">
</form>