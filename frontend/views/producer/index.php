<?php
/* @var $this yii\web\View */
/* @var $producersList frontend\models\Category */

use yii\helpers\Url;
use yii\helpers\Html;
?>
<h1>PRODUCERS LIST</h1> 

<a href="<?php echo Url::to(['producer/add'])?>">
    <?php echo Html::button('add new producer', ['class' => 'btn btn-primary']);?>
</a>
<br><br>

<?php foreach ($producersList as $producer):?>
    <em><?php echo $producer->name . ' (' . $producer->country . ')';?></em><br>
<?php endforeach; 
