<?php
/* @var $this yii\web\View */
/* @var $producer frontend\models\Producer */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<p>
    <b>ADD NEW PRODUCER</b>
</p>

<?php $form = ActiveForm::begin(); ?>
    <?php echo $form->field($producer, 'name'); ?>
    <?php echo $form->field($producer, 'country'); ?>
    <?php echo Html::submitButton('add', ['class' => 'btn btn-primary']);?>
    <a href="<?php echo Url::to(['producer/'])?>">
        <?php echo Html::button('back to producers list', ['class' => 'btn btn-primary']);?>
    </a>
<?php ActiveForm::end();

