<?php

/*
 * @var $db frontend\controllers\DaoController
 * @var $command frontend\controllers\DaoController
 * @var $arrayWithResults frontend\controllers\DaoController
 * @var arr2 frontend\controllers\DaoController
 */

use yii\helpers\Html;

echo Html::beginTag('pre');
var_dump($db);
echo Html::endTag('pre');

echo Html::tag('hr');

echo Html::beginTag('pre');
var_dump($command);
echo Html::endTag('pre');

echo Html::tag('hr');

echo Html::beginTag('pre');
var_dump($arrayWithResults);
echo Html::endTag('pre');

echo Html::beginTag('pre');
var_dump($arr2);
echo Html::endTag('pre');