<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;
?>
<h1>SHOP</h1>

<div>
    <h3>PRODUCT</h3> 
        <a href="<?php echo Url::to(['product/add'])?>">
            <?php echo Html::button('add new product', ['class' => 'btn btn-primary']);?>
        </a>
        <a href="<?php echo Url::to(['product/'])?>">
            <?php echo Html::button('back to product list', ['class' => 'btn btn-primary']);?>
        </a>
        <hr>
</div>
<div>
    <h3>PRODUCER</h3>
        <a href="<?php echo Url::to(['producer/'])?>">
            <?php echo Html::button('back to producers list', ['class' => 'btn btn-primary']);?>
        </a>
        <a href="<?php echo Url::to(['producer/add'])?>">
            <?php echo Html::button('add new producer', ['class' => 'btn btn-primary']);?>
        </a>
        <hr>
</div>
<div>
    <h3>CATEGORY</h3>
        <a href="<?php echo Url::to(['category/add'])?>">
            <?php echo Html::button('add new category', ['class' => 'btn btn-primary']);?>
        </a>
        <a href="<?php echo Url::to(['category/'])?>">
            <?php echo Html::button('back to category list', ['class' => 'btn btn-primary']);?>
        </a>
        <hr>
</div>
