<?php

/* 
 * slider view
 */
use frontend\assets\SliderAsset;

SliderAsset::register($this);
$this->registerJsFile('@web/js/slider/script.js', ['depends'=>[
        SliderAsset::className()
]]);

?>
<link rel="stylesheet" href="css/slider/slider.css">

<ul class="rslides" id="slider">
  <li><img src="files/slider/1.jpg" alt=""></li>
  <li><img src="files/slider/2.jpg" alt=""></li>
  <li><img src="files/slider/3.jpg" alt=""></li>
</ul>






