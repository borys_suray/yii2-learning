<?php
/* @var $this yii\web\View */
/*@ var $productsList[] frontend\models\Product*/

use yii\helpers\Url;
use yii\helpers\Html;
?>

<h1>PRODUCT LIST</h1>

<a href="<?php echo Url::to(['product/add'])?>">
    <?php echo Html::button('add new product', ['class' => 'btn btn-primary']);?>
</a>
<br><br>

<?php foreach ($productsList as $product):?>
    <em><?php echo $product->name;?></em><?php echo ' : ' . $product->price . '$';?><br>
    <small>by <?php echo $product->getProducerName();?></small>
    <hr>
<?php endforeach; 
