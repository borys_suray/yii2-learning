<?php
/* @var $this yii\web\View */
/* @var $product frontend\models\Product */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<h1><b>ADD NEW PRODUCT<b></h1>
            
<?php $form = ActiveForm::begin();?>
<?php echo $form->field($product, 'name'); ?>
<?php echo $form->field($product, 'description'); ?>
<?php echo $form->field($product, 'price'); ?>
<?php echo $form->field($product, 'is_available')->dropDownList([1=>'yes',0=>'no']); ?>
<?php echo $form->field($product, 'category_id')->dropDownList($product->getCategoriesList()); ?>
<?php echo $form->field($product, 'producer_id')->dropDownList($product->getProducersList()); ?>            
<?php echo Html::submitButton('add', ['class' => 'btn btn-primary']);?>
<a href="<?php echo Url::to(['product/'])?>">
    <?php echo Html::button('back to product list', ['class' => 'btn btn-primary']);?>
</a>
<?php ActiveForm::end();
