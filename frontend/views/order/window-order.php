<?php
/**
 * Lesson 11. Window order form
 *
 * @var $model frontend\models\WindowOrder 
 */
if ($model->hasErrors()){
    echo '<pre>';
    print_r($model->getErrors());
    echo '</pre>';
}

if (Yii::$app->session->hasFlash('success')){
    echo Yii::$app->session->getFlash('orderStatus');
}

?>

<form class="form-horizontal" action="" method="post">
    <div class="form-group">
        <h3 style="text-align: center">Order your new window now</h3>
    </div>
    <div class="form-group">
        <label for="width" class="col-sm-2 control-label">Width</label>
        <div class="col-sm-10">
            <input type="string" class="form-control" id="width" name="width"
             placeholder="width" data-toggle="tooltip" title="Choose value from 70 to 210">
        </div>
    </div>
    <div class="form-group">
        <label for="height" class="col-sm-2 control-label">Height</label>
        <div class="col-sm-10">
            <input type="string" class="form-control" id="height" name="height"
             placeholder="height" data-toggle="tooltip" title="Choose value from 100 to 200">
        </div>
    </div>
    <fieldset class="form-group" id="num-cum">        
        <label for="num-cum" class="col-sm-2 control-label">Number of cameras</label>
      <div class="col-sm-10">
          <div class="form-check">
              <input class="form-check-input" type="radio" name="camerasNumber" id="gridRadios1" value="1" checked>
                  <label class="form-check-label" for="gridRadios1">
                  1
                  </label>
          </div>
          <div class="form-check">
              <input class="form-check-input" type="radio" name="camerasNumber" id="gridRadios1" value="2">
                  <label class="form-check-label" for="gridRadios1">
                  2
                  </label>
          </div>
          <div class="form-check">
              <input class="form-check-input" type="radio" name="camerasNumber" id="gridRadios1" value="3">
                  <label class="form-check-label" for="gridRadios1">
                  3
                  </label>
          </div>        
      </div>  
    </fieldset>
    <div class="form-group">
        <label for="flapsTotal" class="col-sm-2 control-label">Total number of flaps</label>
        <div class="col-sm-10">
            <input type="string" class="form-control" id="flapsTotal" name="flapsTotal"
                   placeholder="total number of flaps" data-toggle="tooltip" title="Choose value more or equal to 1">
        </div>
    </div>
    <div class="form-group">
        <label for="flapsSwivel" class="col-sm-2 control-label">Number of swivel flaps</label>
        <div class="col-sm-10">
            <input type="string" class="form-control" id="flapsSwivel" name="flapsSwivel"
                   placeholder="number of swivel flaps" data-toggle="tooltip" title="Choose number of swivel flaps or type 0">
        </div>
    </div>
    <div class="form-group">
        <label for="color" class="col-sm-2 control-label">Color</label>
        <div class="col-sm-10">
            <select name ="color" id="color" class="form-control">
            <option value="white">white</option>
            <option value="brown">brown</option>
            <option value="black">black</option>
            <option value="cofee">cofee</option>
            <option value="snowwhite">snow-white</option>
        </select>
        </div>
    </div>
    <fieldset class="form-group" id="sill">
        <label class="col-sm-2 control-label">Window sill</label>
        <div class="col-sm-10">           
                <label class="checkbox-inline">
                    <input type="checkbox" name="windowsill" value="1" checked>Yes
                </label>
       </div>
    </fieldset>
    <div class="form-group">
        <label for="customerName" class="col-sm-2 control-label">Your name</label>
        <div class="col-sm-10">
            <input type="string" class="form-control" id="customerName" name="customerName" placeholder="type your name">
        </div>
    </div>
    <div class="form-group">
        <label for="customerEmail" class="col-sm-2 control-label">Your email</label>
        <div class="col-sm-10">
            <input type="string" class="form-control" id="customerEmail" name="customerEmail" placeholder="your@email.com">
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="form-control btn btn-success">Confirm</button>
        </div>
    </div>
</form>

<script>
// после загрузки страницы
$(function () {
  // инициализировать все элементы на страницы, имеющих атрибут data-toggle="tooltip", как компоненты tooltip
  $('[data-toggle="tooltip"]').tooltip();
});
</script>

