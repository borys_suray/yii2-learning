<?php

/**
 * Lesson 11. Models
 *  
 * @var $orderData yii\frontend\models\WindowOrder
 */

?>

<table>
    <thead>
        <tr>
            <th>Parameter</th>
            <th>Value</th>
        </tr>
    </thead>
    <?php foreach ($orderData as $attribute => $value): ?>
    <tr>
        <td><?php echo $attribute?></td>
        <td><?php echo $value?></td>
    </tr>
    <?php endforeach; ?>
</table>


