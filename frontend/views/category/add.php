<?php
/* @var $this yii\web\View */
/* @var $category frontend\models\Category */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<p>
    <b>ADD NEW CATEGORY</b>
</p>
<?php $form = ActiveForm::begin(); ?>
<?php echo $form->field($category, 'name'); ?>
<?php echo Html::submitButton('add', ['class' => 'btn btn-primary']);?>
<a href="<?php echo Url::to(['category/'])?>">
    <?php echo Html::button('back to category list', ['class' => 'btn btn-primary']);?>
</a>
<?php ActiveForm::end(); 



