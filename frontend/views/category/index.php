<?php
/* @var $this yii\web\View */
/* @var $categoriesList frontend\models\Category */

use yii\helpers\Url;
use yii\helpers\Html;
?>
<h1>CATEGORIES LIST</h1> 

<a href="<?php echo Url::to(['category/add'])?>">
    <?php echo Html::button('add new category', ['class' => 'btn btn-primary']);?>
</a>
<br><br>

<?php foreach ($categoriesList as $category):?>
    <em><?php echo $category->name;?></em><br>
<?php endforeach; 

