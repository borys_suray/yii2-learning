<?php

/* @var $this yii\web\View */
/* @var $model frontend\models\forms\SearchForm */
/* @var $results = array() */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\helpers\HighlightHelper;

?>

<h1>Search v.2</h1>

<div class="col-md-12">
    <?php $form = ActiveForm::begin(); ?>
        <?=$form->field($model, 'keyword');?>
        <?=Html::submitButton('Go', ['class'=>'btn btn-primary']);?>
    <?php ActiveForm::end(); ?>
</div>
<hr>

<div class="col-md-8">
    <?php if(is_array($results)): ?>    
        <?php foreach ($results as $item):?> 
            <b>
                <?=$item['title']; ?>
            </b>
            <br>
            <?= HighlightHelper::regHighlight($model->keyword, $item['content']); ?>
            <br><hr>
        <?php endforeach;?>            
    <?php else: {echo "<br>Nothing has been found yet";}?>
    <?php endif;?>
</div>
