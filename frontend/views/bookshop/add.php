<?php

/* @var $this yii\web\View */
/* @var $book frontend\models\Book */
/* @var $publishers[] frontend\models\Publisher\getAll()*/

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use dosamigos\datepicker\DatePicker;
?>

<?php 
    if ($book->hasErrors()){
        echo '<pre>';
        print_r($book->getErrors());
        echo '</pre>';
    }
?>


<?php $form = ActiveForm::begin();?>

<?php echo $form->field($book, 'name');?>

<?php echo $form->field($book, 'isbn');?>

<?php echo $form->field($book, 'date_published')->widget(
    DatePicker::className(), [
        // inline too, not bad
         'inline' => true, 
         // modify template for custom rendering
        'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
        ]
]);?>

<?php echo $form->field($book, 'publisher_id')->dropDownList($publishers);?>

<?php echo Html::submitButton('save', ['class' => 'btn btn-primary']);?>

<?php ActiveForm::end();?>



