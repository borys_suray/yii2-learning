<?php
/* @var $this yii\web\View */
/* @var $bookList[] frontend\models\Book */

use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php echo Html::tag('h1', 'Books list')?>
<a href="<?php echo Url::to(['bookshop/add']);?>" class="btn btn-primary">Create book</a>
<br><br>

<?php foreach ($bookList as $book):?>
    <?php echo Html::beginTag('div', ['class' => 'col-md-10'])?>
        <b><?php echo $book->name . ':';?></b>
        <em><?php echo $book->getPublishedData();?></em>
        <?php echo Html::tag('p', $book->getPublisherName());?>
        
        <?php foreach ($book->getAuthors() as $author):?>
            <p><?php echo $author->getAuthorsFullName();?></p>
        <?php endforeach;?>
    <?php echo Html::tag('hr')?>
    <?php echo Html::endTag('div')?>
    <br>
       
<?php endforeach;

