<?php
/*Lesson 13. Helpers. Rendered by frontend\controllers\HtmlHelperController*/

use yii\helpers\Html;

//generate tags
echo Html::tag('p', 'some paragraph');
echo Html::tag('br');
echo Html::tag('p', 'another paragraph');

//build simple table
echo Html::beginTag('table');
echo Html::beginTag('tr');
echo Html::tag('td', 'first cell');
echo Html::tag('td', 'second cell');
echo Html::endTag('tr');
echo Html::beginTag('tr');
echo Html::tag('td', 'third cell');
echo Html::tag('td', 'fourth cell');
echo Html::endTag('tr');
echo Html::endTag('table');

echo Html::tag('br');

$array = [
    '1' => 'Zhitomyr',
    '2' => 'Kyiv',
    '3' => 'Odessa',
    '4' => 'Lviv',
    '5' => 'Berlin',
    '6' => 'Silicon Valley',
    '7' => 'Bangcok',
    '8' => 'Yaremche',
    '9' => 'Kyiv',
    '10' => 'Krakiv',
    '11' => 'Oslo',
];

//generate select
echo Html::dropDownList('city', [], $array);

//generate radiobuttons
echo Html::radioList('city', [], $array);

//generate checkboxes
echo Html::checkboxList('city', [], $array);

//insert image 
echo Html::img('@images/coffee.jpg', ['alt'=>'coffee']);



