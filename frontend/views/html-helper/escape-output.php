<?php

/*Lesson 13. Escape output. Rendered by frontend\controllers\HtmlHelperController*/

/*@var $comments array frontend\controllers\HtmlHelperController*/

use yii\helpers\Html;

foreach ($comments as $comment):?>

<?php echo Html::beginTag('p'); ?>
<?php echo Html::tag('b', Html::encode($comment['author'])); ?>
<?php echo Html::endTag('p'); ?>
<?php echo Html::beginTag('p'); ?>
<?php echo Html::tag('i', Html::encode($comment['text'])); ?>
<?php echo Html::endTag('p'); ?>
<?php echo Html::tag('hr'); ?>

<?php endforeach; ?>


