<?php

/*@var $this yii\web\View*/
/*@var $model frontend\coontrollers\CommentController*/

use yii\helpers\Html;

if ($model->hasErrors()){
    echo '<pre>';
    print_r($model->getErrors());
    echo '</pre>';
}

if (Yii::$app->session->hasFlash('success')){
    echo Yii::$app->session->getFlash('Comment successfully added');
    header('Location: ');
}
?>

<?php 

    $commentsArray = $model->getCommentsList();
    
    foreach ($commentsArray as $comment):
?>
<div>
    <i style="font-size: small"><?php echo(Html::encode($comment['name'])). ':';?></i>
    <p><?php echo(Html::encode($comment['comment']));?></p>
    <hr>
</div>

<?php endforeach; ?>

<form class="form-horizontal" action="" method="post">
    <div class="form-group">
        <h3 style="text-align: center">Add your comment</h3>
    </div>
    <div class="form-group">
        <label for="customerName" class="col-sm-2 control-label">Your name</label>
        <div class="col-sm-10">
            <input type="string" class="form-control" id="name" name="name" placeholder="type your name">
        </div>
    </div>
    <div class="form-group">
        <label for="customerEmail" class="col-sm-2 control-label">Your email</label>
        <div class="col-sm-10">
            <input type="string" class="form-control" id="email" name="email" placeholder="your@email.com">
        </div>
    </div>
    <div class="form-group">
        <label for="comment" class="col-sm-2 control-label">Comment:</label>
         <div class="col-sm-10">
             <textarea class="form-control" rows="5" id="comment"name="comment"></textarea>
         </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success">Confirm</button>
        </div>
    </div>
</form>

