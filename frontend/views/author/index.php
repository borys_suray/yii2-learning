<?php
/* @var $this yii\web\View */
/* @var $authorsList[] frontend/models/Author */

use yii\helpers\Url;
use yii\helpers\Html;
?>
<h1>Authors</h1>
<br>

<a href="<?php echo Url::to(['author/create'])?>">
    <?php echo Html::button('create new author', ['class'=>'btn btn-primary']) ?>
</a><br><br>

<table class="table table-condensed">
    <tr>
        <th>ID</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
    
    <?php foreach ($authorsList as $author):?>
        <tr>
            <td><?php echo ($author->id)?></td>
            <td><?php echo ($author->first_name)?></td>
            <td><?php echo ($author->last_name)?></td>
            <td><a href = "<?php echo Url::to(['author/update', 'id' => $author->id]);?>">edit</a></td>
            <td><a href = "<?php echo Url::to(['author/delete', 'id' => $author->id]);?>">delete</a></td>
        </tr>
    <?php endforeach;?>
    
</table>


