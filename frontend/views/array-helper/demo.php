<?php

/**
 * @var $employees array;
 */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$emails = ArrayHelper::getColumn($employees, 'email');

echo 'our mails: ' . implode(', ', $emails);

echo '<br><br>';

//Array helper with html-lists

/*$array = [
    '1' => 'Zhitomyr',
    '2' => 'Kyiv',
    '3' => 'Odessa',
    '4' => 'Lviv',
    '5' => 'Berlin',
    '6' => 'Silicon Valley',
    '7' => 'Bangcok',
    '8' => 'Yaremche',
    '9' => 'Kyiv',
    '10' => 'Krakiv',
    '11' => 'Oslo',
];*/ //EXAMPLE

$listData = ArrayHelper::map($employees, 'id', 'email');

echo '<pre>';
print_r($listData);
echo '</pre>';

//generate select
echo Html::dropDownList('emails', [], $listData);