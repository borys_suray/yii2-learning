<?php

namespace frontend\widgets\newsList;

use Yii;
use yii\base\Widget;
use frontend\models\Test;


/**
 * Lesson 12. Handles getting newslist from database
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class NewsList extends Widget {
    public $show_limit = null;
    

    public function run() {
        
        $max = Yii::$app->params['maxNewsNumber'];
        if($this->show_limit){
            $max = $this->show_limit;
        }
        $newsList = Test::getNewsList($max);
        
        return $this->render('block', [
            'list' => $newsList,
        ]);
    }
}
