<?php use yii\helpers\Url; ?>

<?php foreach ($list as $item): ?>

    <a href="<?php echo Url::to(['test/view', 'id' => $item['id']]);?>">
        <h2><?php echo $item['title'];?></h2>
    </a>
    <p><b><?php echo $item['description']; ?></b></p>

    <br>

    <p><?php echo $item['content']; ?></p>

    <hr>
    <?php

        #$formatter = Yii::$app->formatter;
        echo Yii::$app->formatter->asDatetime('now');
    ?>


<?php endforeach; ?>