<?php

namespace frontend\models\events;

use yii\base\Event;
use frontend\models\User;
use common\components\UserNotificationInterface;
/**
 * UserRegisteredEvent
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class UserRegisteredEvent extends Event implements UserNotificationInterface 
{
    
    /**
     * @var User $user 
     */
    public $user;
    
    /**
     * @var string $subject
     */
    public $subject;
    
    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }
    
    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->user->email;
    }            
}
