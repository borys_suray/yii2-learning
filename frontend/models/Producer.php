<?php

namespace frontend\models;

/**
 * This is the model class for table "producer".
 *
 * @property int $id
 * @property string $name
 * @property string $country
 */
class Producer extends \yii\db\ActiveRecord
{
    //dcclare scenario constants
    const SCENARIO_PRODUCER_ADD = 'producer_add';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{producer}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'country'], 'string', 'min' => 2, 'max' => 255],
            [['name', 'country'], 'required'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function scenarios() {
        return [self::SCENARIO_PRODUCER_ADD =>[
            'name', 'country',
        ]]; 
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'country' => 'Country',
        ];
    }
}
