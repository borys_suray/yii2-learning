<?php

namespace frontend\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/**
 * handles the simple search from the "news" table by keyword
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class NewsSearch 
{
    public function simpleSearch($keyword){
        
        $encodedKeyword = Html::encode($keyword);
        $sql = "SELECT * FROM news WHERE content LIKE '%$encodedKeyword%' LIMIT 20";
        
        return Yii::$app->db->createCommand($sql)->queryAll();
    }
    
    public function fullTextSearch($keyword){
        
        $encodedKeyword = Html::encode($keyword);
        $sql = "SELECT * FROM news WHERE MATCH (content) AGAINST ('$encodedKeyword') LIMIT 200";
        
        return Yii::$app->db->createCommand($sql)->queryAll();
    }   
    
    public function searchAdvanced($keyword){
        
        $encodedKeyword = Html::encode($keyword);
        $sql = "SELECT * FROM idx_news_content WHERE MATCH ('$encodedKeyword')LIMIT 200 OPTION ranker = WORDCOUNT;";
        
        $data = Yii::$app->sphinx->createCommand($sql)->queryAll();
        
        $ids = ArrayHelper::map($data, 'id', 'id');
       
        
        $preResult = News::find()->where(['id'=>$ids])->asArray()->all();
        
        $reindexedResult = ArrayHelper::index($preResult, 'id');
        
        $returnSearchResult = [];
        
        foreach ($ids as $element){
           $returnSearchResult[] = [
               'id' => $element,
               'title' => $reindexedResult[$element]['title'],
               'content' => $reindexedResult[$element]['content'],
           ];
        } 
        
        return $returnSearchResult;
    }   
}
