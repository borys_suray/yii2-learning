<?php

namespace frontend\models\forms;

use yii\base\Model;
use frontend\models\NewsSearch;

/**
 * a model handles searching procedure
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class SearchForm extends Model{
    //set attributes
    public $keyword;
    
    //set validation rules
    public function rules() {
        return[
            ['keyword', 'trim'],
            ['keyword', 'required'],            
            ['keyword', 'string', 'min'=>3],
        ];
    }
    
    public function search()
    {
        if($this->validate()){
            $model = new NewsSearch();
            return $model->fullTextSearch($this->keyword);
        }
    }
    
    public function searchAdvanced()
    {
        if($this->validate()){
            $model = new NewsSearch();
            return $model->searchAdvanced($this->keyword);
        }
    }
}
