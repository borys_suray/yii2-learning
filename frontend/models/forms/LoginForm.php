<?php

namespace frontend\models\forms;

use yii\base\Model;
use frontend\models\User;
use Yii;

/**
 * a model handles loginning procedure
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class LoginForm extends Model {
    
    //set attributes
    public $username;
    public $password;
    
    //set validation rules
    public function rules() {
        return[
            ['username', 'trim'],
            ['username', 'required'],            
            ['password', 'required'],
            ['password', 'validatePassword'],
        ];
    }
    
    /**
     *  function handles loginning 
     * 
     *  @return bool
     */
    public function login(){
        
        if($this->validate()){
            
            $user = User::findUserByUsername($this->username);
            return Yii::$app->user->login($user);
        }
        return false;
    }
    
    /**
     * password validator
     * 
     */
    public function validatePassword($attribute, $params){
        
        $user = User::findUserByUsername($this->username);
        
        if (!$user || !$user->checkPassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password');
        }
    }
}