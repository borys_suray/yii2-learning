<?php

namespace frontend\models\forms;

use yii\base\Model;
use frontend\models\User;
use Yii;
use frontend\models\events\UserRegisteredEvent;

/**
 * a model for the validation of signup from data and writing this data to the table "user"
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class SignupForm extends Model {
    
    //set attributes
    public $username;
    public $email;
    public $password;
    
    //set validation rules
    public function rules() {
        return[
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['username', 'unique', 'targetClass'=> User::className()],
            
            ['email', 'trim'],
            ['email', 'email'],
            ['email', 'required'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass'=> User::className()],
            
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }
    
    /**
     *  function validates user's input and writes a new user to the table "user"
     * 
     *  @return User|null
     */
    public function save(){
        
        if ($this->validate()){
            
            $user = new User();
            
            $user->username = $this->username;
            $user->email = $this->email;
            $user->created_at = $time = time();
            $user->updated_at = $time;
            $user->auth_key = Yii::$app->security->generateRandomString();
            $user->password_hash = Yii::$app->security->generatePasswordHash($this->password);
            
            if ($user->save()){
                
                $event = new UserRegisteredEvent();
                $event->user = $user;
                $event->subject = 'A new user registered';
               
                
                $user->trigger(User::USER_REGISTRED, $event);
                //Yii::$app->EmailService->notifyUser($user, 'Welcome');
                //Yii::$app->EmailService->notifyAdmin('New user registered');
          
                
                return $user;
            }
        }
    }
}
