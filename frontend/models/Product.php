<?php

namespace frontend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $price
 * @property int $is_available
 * @property int $category_id
 * @property int $producer_id
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{product}}';
    }
    
    //dcclare scenario constants
    const SCENARIO_PRODUCT_ADD = 'product_add';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string'],
            [['price', 'is_available', 'category_id', 'producer_id'], 'integer'],
            [['description'], 'string', 'max' => 255],
            [['name', 'description', 'price', 'is_available', 'category_id', 'producer_id'], 'required'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function scenarios() {
        return [self::SCENARIO_PRODUCT_ADD =>[
            'name', 'description', 'price', 'is_available', 'category_id', 'producer_id',
        ]]; 
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'image' => 'Image',
            'price' => 'Price',
            'is_available' => 'Is Available',
            'category_id' => 'Category ID',
            'producer_id' => 'Producer ID',
        ];
    }
    
    /**
     * to get the list of categories from the table "category"
     * 
     * @return array[]
     */
    public function getCategoriesList(){
        
        $query = 'SELECT * FROM category;';
        $result = Yii::$app->db->createCommand($query)->queryAll();
        
        return ArrayHelper::map($result, 'id', 'name');
    }
    
    /**
     * to get the list of producers from the table "producer"
     * 
     * @return array[]
     */
    public function getProducersList(){
        
        $query = 'SELECT * FROM producer;';
        $result = Yii::$app->db->createCommand($query)->queryAll();
        
        return ArrayHelper::map($result, 'id', 'name');
    }
    
    /**
     * to get the product's producer from the table "producer"
     * 
     * @return object frontend\models\producer
     */
    public function getProducer()
    {
        
        return ($this->hasOne(Producer::className(), ['id' => 'producer_id'])->one());
    }
    
    /**
     * to get the product's producer name from the table "producer"
     * 
     * @return string
     */
    public function getProducerName()
    {
        return ($producer = $this->getProducer()) ? $producer->name : 'Unknown producer';
    }
}
