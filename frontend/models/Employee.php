<?php

namespace frontend\models;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Lesson 11. Test Model
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class Employee extends Model
{
    //dcclare scenario constants
    const SCENARIO_EMPLOYEE_REGISTER = 'employee_register';
    const SCENARIO_EMPLOYEE_UPDATE = 'employee_update';
    
    //declare model attributes
    public $firstName;
    public $middleName;
    public $lastName;
    public $email;
    public $birthDate;
    public $startDate;
    public $cityId;
    public $position;
    public $idCode;

    
    //describe scenarios
    public function scenarios() {
        return [
            self::SCENARIO_EMPLOYEE_REGISTER => [
                'firstName', 'middleName', 'lastName', 'email', 'birthDate', 'startDate', 'cityId', 'position', 'idCode'
                ],
            self::SCENARIO_EMPLOYEE_UPDATE => ['firstName', 'middleName', 'lastName'],
        ];
    }
    //set validation rules
    public function rules() {
        return [
            [['firstName', 'lastName', 'email'], 'required'],
            [['firstName'], 'string', 'min' => 2],
            [['lastName'], 'string', 'min' => 2],
            [['email'], 'email'],
            [['middleName'], 'required', 'on' => self::SCENARIO_EMPLOYEE_UPDATE],
            [['birthDate'], 'date', 'format' => 'php:Y-m-d', 'on' => self::SCENARIO_EMPLOYEE_REGISTER],
            [['startDate'], 'date', 'format' => 'php:Y-m-d', 'on' => self::SCENARIO_EMPLOYEE_REGISTER],
            [['startDate', 'position', 'idCode'], 'required', 'on' => self::SCENARIO_EMPLOYEE_REGISTER],
            [['cityId'],  'integer', 'on' => self::SCENARIO_EMPLOYEE_REGISTER],
            [['position'], 'string', 'on' => self::SCENARIO_EMPLOYEE_REGISTER], 
            [['idCode'], 'string', 'length' => [10]],
            [['idCode'], 'match', 'pattern' => '/^[0-9]{10}$/']
        ];
    }
    
    //writes data to database
    public function save(){
        
        $sql = "INSERT INTO employee "
                . "(id, firstName, middleName, lastName, email, birthDate, startDate, cityId, position, idCode) "
                . "VALUES ("
                . "null, '{$this->firstName}', '{$this->middleName}', '{$this->lastName}', "
                . "'{$this->email}', '{$this->birthDate}', '{$this->startDate}', '{$this->cityId}', "
                . "'{$this->position}', '{$this->idCode}')";
        
        Yii::$app->db->createCommand($sql)->execute();
        
        return true;
    }
    
    //updates data in database
    public function update(){
        return true;
    }
    
    public static function getEmployeeList(){
        
        $sql = 'SELECT * FROM employee WHERE true';
        
        return Yii::$app->db->createCommand($sql)->queryAll();
    }
    
    public function getCitiesList(){
        
        $sql = 'SELECT * FROM cities WHERE true';
        
        $result =  Yii::$app->db->createCommand($sql)->queryAll();
        
        return ArrayHelper::map($result, 'cityId', 'city');
    }
    
    
    
}
