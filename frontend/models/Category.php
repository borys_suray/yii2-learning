<?php

namespace frontend\models;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $name
 */
class Category extends \yii\db\ActiveRecord
{
    //dcclare scenario constants
    const SCENARIO_CATEGORY_ADD = 'category_add';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{category}}';
    }
    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'min' => 2, 'max' => 255],
            [['name'], 'required'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function scenarios() {
        return [self::SCENARIO_CATEGORY_ADD =>[
            'name'
        ]]; 
    }

    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
