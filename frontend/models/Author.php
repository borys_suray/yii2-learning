<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "author".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $birthdate
 * @property int $rating
 */
class Author extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{author}}';
    }
    
    //set validation rules
    public function rules() 
    {
        return[
            [['first_name', 'last_name'], 'required'],
            [['first_name', 'last_name'], 'string' , 'max'=>25],
            [['birthdate'], 'date', 'format'=>'php:Y-m-d'],
            [['rating'], 'integer'],
        ];
        
    }

        public function getAuthorsFullName()
    {
        if($this->first_name && $this->last_name) {
            return $this->first_name . ' ' . $this->last_name;
        } elseif ($this->first_name && !$this->last_name) {
            return $this->first_name;
        } elseif ($this->last_name && !$this->first_name) {
            return $this->last_name;
        } else {
            return 'Unknown author';
        } 
    }
}
