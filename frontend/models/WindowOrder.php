<?php

namespace frontend\models;

use yii\base\Model;
use Yii;

/**
 * Lesson 11. Model handles window order
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class WindowOrder extends Model{
     
    //declare scenarios
    const SCENARIO_WINDOW_ORDER_ORDER = 'window_order';
    
    //declare form attribures
    public $width;
    public $height;
    public $camerasNumber;
    public $flapsTotal;
    public $flapsSwivel;
    public $color;
    public $windowsill = 0;
    public $customerName;
    public $customerEmail;
    
    //describe scenarios
    public function scenarios() {
        return [
            self::SCENARIO_WINDOW_ORDER_ORDER => [
                'width', 'height', 'camerasNumber', 'flapsTotal', 'flapsSwivel', 'color',  'windowsill',
                'customerName', 'customerEmail'
            ],
        ];
    }
    
    //set validation rules
    public function rules() {
        return [
            [['width', 'height', 'camerasNumber', 'flapsTotal', 'flapsSwivel', 'color', 'windowsill',
                'customerName', 'customerEmail'], 'required'],
            [['width'], 'integer', 'min' => '70', 'max' => '210'],
            [['height'], 'integer', 'min' => '100', 'max' => '200'],
            [['camerasNumber'], 'in', 'range' => [1, 2 ,3]],
            [['flapsTotal'], 'integer', 'min' => 1],
            [['flapsSwivel'], 'integer', 'min' => 0],
            [['flapsSwivel'], 'compare', 'compareAttribute' => 'flapsTotal', 'operator' => '<=', 'type' => 'number'],
            [['color'], 'match', 'pattern' => '/^[a-zA-Z-]+$/'],
            [['customerName'], 'string', 'min' => 1],
            [['customerEmail'], 'email'],            
        ];
    }
    
    //mail to site admin
    public function sendOrderToAdmin(){
        
        $adminEmail = Yii::$app->params['adminEmail'];  
        
        $result = Yii::$app->mailer->compose('/order/order-table',
                    ['orderData' => $this->attributes])
                ->setFrom('boryssuray@gmail.com')
                ->setTo($adminEmail)
                ->setSubject('Order')
                ->send();
        return $result ? true : false;
        
    }

    
}
