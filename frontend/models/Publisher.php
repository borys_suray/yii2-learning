<?php

namespace frontend\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "publisher".
 *
 * @property int $id
 * @property string $name
 * @property string $date_registered
 * @property int $identity_number
 */
class Publisher extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{publisher}}';
    }
    
     //set validation rules
    public function rules() 
    {
        return[
            [['name', 'identity_number'], 'required'],
            [['name'], 'string' , 'max'=>40],
            [['date_registered'], 'date', 'format'=>'php:Y-m-d'],
        ];
        
    }
    
    /**
     * to get publishers list array['id'=>'name] from the table 'publisher'
     * 
     * @return array
     */
    public static function getAll()
    {
        $list = self::find()->asArray()->all();
        
        return ArrayHelper::map($list, 'id', 'name');        
    }
}
