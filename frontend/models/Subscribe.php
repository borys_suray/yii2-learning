<?php

namespace frontend\models;

use yii\base\Model;
use Yii;

/**
 * Lesson 11. 
 * <p>To validate data and write them to database</p>
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class Subscribe extends Model{
    
    public $email;
    
    /**
     * to describe rules for validation
     * @return array
     */
    public function rules(){
        
        return [
            [['email'], 'email'],
            [['email'], 'required'],
        ];
    }
    
    public function save(){
        
        $sql = "INSERT INTO subscriber (id, email) " .
               "VALUES (null, '{$this->email}')";
               
        return Yii::$app->db->createCommand($sql)->execute() ;
    }
}
