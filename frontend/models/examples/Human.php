<?php

namespace frontend\models\examples;
use yii\base\Model;
/**
 * Lesson 11. Demonstrate traits
 *
 * @author aser
 */
class Human extends Model 
{
    use FirstTrait;
}
