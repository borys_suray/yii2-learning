<?php

namespace frontend\models\examples;

/**
 * Lesson 11. Try traits
 *
 * @author aser
 */
trait FirstTrait {
    
    public function walk(){
        echo 'I\'m walking better<br>';
    }
}
