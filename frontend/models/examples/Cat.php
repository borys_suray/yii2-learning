<?php

namespace frontend\models\examples;
use yii\base\Model;
/**
 * Lesson 11. Demonstrate traits
 *
 * @author aser
 */
class Cat extends Model  
{
    use FirstTrait;
}
