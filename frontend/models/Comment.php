<?php

namespace frontend\models;

use yii\base\Model;
use Yii;


/**
 * Lesson 13. Model for managing comments 
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class Comment extends Model{
    
    //declare scenarios
    const SCENARIO_COMMENT_ADD = 'comment_add';
    
    //declare form attribures
    public $name;
    public $email;
    public $comment;
    
    //describe scenario
    public function scenarios() {
        return [
            self::SCENARIO_COMMENT_ADD => [
                'name', 'email', 'comment',
            ],
        ];
    }
    
    //set rules
    public function rules() {
        return[
            [['name', 'email', 'comment'], 'required'],
            [['name'], 'string', 'min' => 2],
            [['email'], 'email'],
            [['comment'], 'string', 'max' => 256],
        ];
    }


    public function getCommentsList(){
        
        $sql = "SELECT * FROM comments WHERE 1";
        return Yii::$app->db->createCommand($sql)->queryAll();
    }
    
    public function add(){
        
        $sql = "INSERT INTO comments (id, name, email, comment) VALUES ("
                . "null, '{$this->name}', '{$this->email}', '{$this->comment}');";
        
        return Yii::$app->db->createCommand($sql)->execute() ? true : false;
    }
}
