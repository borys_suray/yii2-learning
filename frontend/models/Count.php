<?php

namespace frontend\models;

use Yii;


/**
 * @author borys suray <suryborys@gmail.com>
 */
class Count {
    
    /**
     * @return integer|boolean
     */
    public static function countItems(){
        
        $sql = 'SELECT COUNT(*) AS number FROM yii2advanced.news';
        
        $result = Yii::$app->db->createCommand($sql)->queryOne();
        
        if(!isset($result['number'])){
            return false;
        }
          
        return intval($result['number']);   
    }
}
