<?php

namespace frontend\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * Lesson 15. Active Record
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class Book extends ActiveRecord{
    
    public static function tableName() {
        
        return '{{book}}';
    }
    
    public function rules() {
        return [
            [['name', 'publisher_id'], 'required'],
            [['date_published'], 'date', 'format' => 'php:Y-m-d'],
            [['isbn'], 'integer'],
        ];
    }
    
    public function getPublishedData(){
        
        return ($this->date_published) ? Yii::$app->formatter->asDate($this->date_published) : 'no information';
    }
    
    public function getPublisher(){
        
        return $this->hasOne(Publisher::className(), ['id' => 'publisher_id'])->one();
    }
    
    public function getPublisherName(){
        
        return ($publisher = $this->getPublisher()) ? $publisher->name : 'No information';
    }
    
    public function getBookToAuthorRelations(){
        
        return $this->hasMany(BookToAuthor::className(), ['book_id' => 'id']);
    }
    
    public function getAuthors(){
        
        return $this->hasMany(Author::className(), ['id' => 'author_id'])->via('bookToAuthorRelations')->all();
    }
}
