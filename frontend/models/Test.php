<?php

namespace frontend\models;

use Yii;

/* 
 * @author borys suray <suryborys@gmail.com>
 */
class Test
{
    /**
     * @return array
     */
    public static function getNewsList($limit=null){
        if(!$limit){
            $limit = Yii::$app->params['maxNewsNumber'];
        }else{
            $limit = intval($limit);
        }
        
        $query = 'SELECT * FROM yii2advanced.news LIMIT ' . $limit;
        $result = Yii::$app->db->createCommand($query)->queryAll();
        
        if (!empty($result) && is_array($result)){
             
            foreach ($result as &$item){
                
                $item['content'] = Yii::$app->stringHelper->getShort($item['content'], 220);
            }
        }
        
        return $result;
    }
    
    /**
     * @param integer $id
     * @return mixed
     */
    public static function getNewsItemById($id){
        
        $id = intval($id);
        
        $query = 'SELECT * FROM yii2advanced.news WHERE id=' . $id;
        $result = Yii::$app->db->createCommand($query)->queryOne();
        
        if (is_array($result)){
            return $result;
        } else {
            return false;
        }
        
    }
}
