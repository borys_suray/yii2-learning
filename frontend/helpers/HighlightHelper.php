<?php

namespace frontend\helpers;

/**
 * HighlightHelper
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class HighlightHelper
{
    
    public static function highlight($keyword, $content)
    {
        return str_replace($keyword, '<mark style="background-color:yellow"><b>' . $keyword . '</b></mark>', $content);
    }
    
    public static function regHighlight($keyword, $content) 
    {
        $pattern_up = '~' . ucfirst($keyword) . '~';
        $replacement = '<mark style="background-color:yellow"><b>' . ucfirst($keyword) . '</b></mark>';
                
        $ucString = preg_replace($pattern_up, $replacement, $content);
        
        $pattern_low = '~' . lcfirst($keyword) . '~';
        $replacement = '<mark style="background-color:yellow"><b>' . lcfirst($keyword) . '</b></mark>';
        
        return preg_replace($pattern_low, $replacement, $ucString);
    }
}
