<?php

namespace common\components;

use yii\base\Component;
use Yii;
use common\components\UserNotificationInterface;

/**
 * Email Service
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
class EmailService extends Component {
    
    public function notifyUser(UserNotificationInterface $event){
        
        Yii::$app->mailer->compose()
                ->setFrom('boryssuray@gmail.com')
                ->setTo($event->getEmail())
                ->setSubject($event->getSubject())
                ->send();
    }
    
    public function notifyAdmin(UserNotificationInterface $event){
        
        
        $adminEmail = Yii::$app->params['adminEmail'];
        
        Yii::$app->mailer->compose()
                ->setFrom('boryssuray@gmail.com')
                ->setTo($adminEmail)
                ->setSubject($event->getSubject())
                ->send();
    }
}
