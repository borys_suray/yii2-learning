<?php

namespace common\components;

use Yii;

/**
 * To cut string 
 *
 * @author borys suray <surayborys@gmail.com>
 */
class StringHelper {
    
    protected $limit;
    
    public function __construct() {
        $this->limit = Yii::$app->params['shortTextLimit'];
    }
    
    /**
     * @param string $str
     * @param mixed $limit
     * @return string
     */
    public function getShort($str, $limit = null){
            
            if($limit === null){
                $limit = $this->limit;
            }
  
        return substr($str, 0, $limit) . '...';
    }
}
