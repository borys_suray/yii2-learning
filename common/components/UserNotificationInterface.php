<?php


namespace common\components;

/**
 * UserNotificationInterface
 *
 * @author Borys Suray <surayborys@gmail.com>
 */
interface UserNotificationInterface 
{
    public function getEmail();
    
    public function getSubject();
}
